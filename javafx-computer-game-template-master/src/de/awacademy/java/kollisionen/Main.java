package de.awacademy.java.kollisionen;

import de.awacademy.java.kollisionen.model.*;
import de.awacademy.java.kollisionen.model.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import java.io.IOException;


public class Main extends Application {

    private Timer timer;

    @Override
    public void init() throws Exception {
        // Lädt den Highscore
        try {
            Highscore.getInstance().loadScore();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        // Lädt die Musik und spiel ab
        Model model = new Model();
        String musicFile = "/musik/POL-foggy-forest-short.wav";
        model.playMusic(musicFile);
    }

    @Override
    public void start(Stage stage) throws Exception {

        HBox root = new HBox();

        Group group = new Group();
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);
        root.getChildren().add(group);
        group.getChildren().addAll(canvas);

        Scene scene = new Scene(root, 1200, 600);
        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Model model = new Model();
        Graphics graphics = new Graphics(model, gc);
        GraphicsRightPanel rightPanel = new GraphicsRightPanel(root, model);
        timer = new Timer(model, graphics, rightPanel);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(event ->
                inputHandler.onKeyPressed(event.getCode()));

        scene.setOnKeyReleased(event ->
                inputHandler.onKeyReleased(event.getCode()));
    }

    @Override
    public void stop() throws Exception {
        // Speichert ab, wenn es ein neues Highscore gibt.
        try {
            Highscore.getInstance().storeScore();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        timer.stop();
        super.stop();
    }


    public static void main(String[] args) {
        Application.launch(args);
    }

}
