package de.awacademy.java.kollisionen;

import de.awacademy.java.kollisionen.model.Model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Highscore {

    static int highscore;
    private static Highscore instance = new Highscore(); // InstanzErstellung
    private static String filename = "Highscore.txt";

    public static Highscore getInstance() {
        return instance;
    }

    /** Lädt zu Beginn des Spiels den Highscore aus Highscore.txt
     *
     * @throws IOException
     */
    public void loadScore() throws IOException {
        Path path = Paths.get(filename);
        BufferedReader bufferedReader = Files.newBufferedReader(path);
        String input;

        try {
            while((input = bufferedReader.readLine()) != null){
                System.out.println(input);
                highscore = Integer.parseInt(input);
            }
        } finally {
            if(bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }

    /**Erstellt bei nichtexistieren die Highscore.txt - Falls sie existiert, wird bei
     * der Erstellung eines neuen Highscores, die alte überschrieben
     *
     * @throws IOException
     */
    public void storeScore() throws IOException {
        int newHighscore = Model.parcelCount;
        Path path = Paths.get(filename);
        if(newHighscore > highscore){
            BufferedWriter bufferedWriter = Files.newBufferedWriter(path);
            try {
                    String newHighscoreString = String.valueOf(newHighscore);
                    System.out.println("New Highscore: " + newHighscore);
                bufferedWriter.write(newHighscoreString);
            } finally {
                if(bufferedWriter != null) {
                    bufferedWriter.close();
                }
        }
    }
}

    public int getHighscore() {
        return highscore;
    }


}
