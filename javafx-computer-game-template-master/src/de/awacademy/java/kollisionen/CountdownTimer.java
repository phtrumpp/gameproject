package de.awacademy.java.kollisionen;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class CountdownTimer extends Pane {

    private Timeline animation;
    private static int tmp = 60;
    private String string;

    Label label = new Label("60");

    public CountdownTimer() {
        label.setFont(javafx.scene.text.Font.font(50));
        label.setTranslateX(50);
//       label.setTranslateY(0);

        getChildren().add(label);

        animation = new Timeline(new KeyFrame(Duration.seconds(1), e -> timeLabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
    }

    private void timeLabel() {
        if(tmp > 0) {
           tmp--;
        }
        string = tmp +"";
        label.setText(string);

    }

    public static int getTmp() {
        return tmp;
    }

    public static void setTmp(int tmp) {
        CountdownTimer.tmp = tmp;
    }
}
