package de.awacademy.java.kollisionen.model;

public class RetourKutsche {

    // Wir erstellen unser PostAuto, welches als eingefärbtes Rechteck auf dem Canvas unseres Spiels zu sehen ist.
    private final int X = 5;
    private final int Y = 525;

    public int getXRetourKutsche() {
        return X;
    }

    public int getYRetourKutsche() {
        return Y;
    }

    public int getHRetourKutsche() {
        return 70;
    }

    public int getWRetourKutsche() {
        return 120;
    }
}
