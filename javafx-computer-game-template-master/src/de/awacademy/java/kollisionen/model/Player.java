package de.awacademy.java.kollisionen.model;

public class Player {
    private int x;
    private int y;
    // Zusaetzlich wurden beide Variablen initalisiert, damit diese im Programm verändert werden koennen
    private int speedOfPlayer = 20;
    private int numberOfParcels = 0;
    private boolean movedUp = false;
    private boolean movedRight = false;
    private boolean movedLeft = false;
    private boolean movedDown = false;

    //Die werden benötigt um die Move() Methode anzupassen.
    AndereMauer andereMauer = new AndereMauer();
    BerlinerMauer berlinerMauer = new BerlinerMauer();
    RetourKutsche retourKutsche = new RetourKutsche();
    PaketShop paketShop = new PaketShop();


    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /** Methoden mit der man den Spieler bewegt
     * @param dx
     * @param dy
     */
    public void move(int dx, int dy) {
        if(     (x+dx) < (Model.WIDTH-10)  &&
                (x+dx) > 10 &&
                (y+dy) < (Model.HEIGHT-10) &&
                (y+dy) > 10
        ) {
            //Abfragen prüfen, ob ein anderes Objekt da ist. Falls ja, wird der Player nicht bewegt.
            if (y+dy > andereMauer.getYAndereMauer()-16 && x+dx > andereMauer.getXAndereMauer()-9) {
                //nichts passiert
            } else if (y+dy < (berlinerMauer.getYBerlinerMauer()+ berlinerMauer.getHBerlinerMauer()+16) && x+dx < (berlinerMauer.getXBerlinerMauer() + berlinerMauer.getWBerlinerMauer()+10)) {
                //nichts passiert
            } else if(y+dy > retourKutsche.getYRetourKutsche()-16 && x+dx < retourKutsche.getXRetourKutsche() + retourKutsche.getWRetourKutsche()+9) {
                //nichts passiert
            } else if (y+dy < (paketShop.getYPaketShop()+paketShop.getHPaketShop()+16) && x+dx > (paketShop.getXPaketShop() -13)) {
                //nichts passiert
            } else {
            this.x += dx;
            this.y += dy; }

        }
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * nimmt eine Box auf. Kann höchstens nur 3 Boxen aufnehmen
     */
    public void takeAParcel() {
        if (numberOfParcels < 3) {
            speedOfPlayer -=2;
            numberOfParcels++;

        }

    }

    /**
     * gibt die aufgenommenen Boxen beim Wagen wieder ab.
     * Pro abgegebene Box steigt die Anzahl der gesammelten Boxen um 1
     */
    public void deliverAParcel() {
        if (numberOfParcels>0) {
        speedOfPlayer+=2;
        numberOfParcels--;
        Model.parcelCount++;
        }
    }

    public int getNumberOfParcels() {
        return numberOfParcels;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return 40;
    }

    public int getH() {
        return 40;
    }

    public int getSpeedOfPlayer() {
        return speedOfPlayer;
    }

    public void setSpeedOfPlayer(int speedOfPlayer) {
        this.speedOfPlayer = speedOfPlayer;
    }

    public void setNumberOfParcels(int numberOfParcels) {
        this.numberOfParcels = numberOfParcels;
    }

    public boolean isMovedUp() {
        return movedUp;
    }

    public void setMovedUp(boolean movedUp) {
        this.movedUp = movedUp;
    }

    public boolean isMovedRight() {
        return movedRight;
    }

    public void setMovedRight(boolean movedRight) {
        this.movedRight = movedRight;
    }

    public boolean isMovedLeft() {
        return movedLeft;
    }

    public void setMovedLeft(boolean movedLeft) {
        this.movedLeft = movedLeft;
    }

    public boolean isMovedDown() {
        return movedDown;
    }

    public void setMovedDown(boolean movedDown) {
        this.movedDown = movedDown;
    }
}
