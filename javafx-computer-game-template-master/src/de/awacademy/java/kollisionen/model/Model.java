package de.awacademy.java.kollisionen.model;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Model {


    // Hier legen wir die Maße unseres Canvas fest, in dem das Spiel ausgeführt wird.
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    public static int parcelCount = 0;
    // Konstante Anzahl der Autos hinzugefügt
    private final int ANZAHLAUTOS = 4;
    private List<Car> cars = new LinkedList<>();

    public int Highscore;

    private Player player;
    private PaketShop paketshop = new PaketShop();
    private RetourKutsche retourKutsche = new RetourKutsche();

    private boolean gameOver = false; // @Mareike "Game Over" anzeigen, wenn Player hit
    private int countAnimation = 0;

    public List<Car> getCars() {
        return cars;
    }

    public Player getPlayer() {
        return player;
    }


    public Model() {
        Random random = new Random();
        int autoAbstand = 160;
        for (int i = 0; i < (ANZAHLAUTOS / 2); i++) {

            this.cars.add(
                    new Car(
                            0, autoAbstand, 0.1f + random.nextFloat() * 0.2f // k = feste Y-Koordinate
                    )
            );
            autoAbstand += 100;

        }

            // @Mareike Änderung, damit zwei der Autos in die andere Richtung fahren.
        for (int j = 0; j < (ANZAHLAUTOS / 2); j++) {

            this.cars.add(
                    new Car(
                            1000, autoAbstand, -0.1f + random.nextFloat() * -0.2f // k = feste Y-Koordinate
                    )
                );

                autoAbstand += 100;
        }

        this.player = new Player(153, 570);
    }

    public void update(long elapsedTime) {
        for (Car car : cars) {
            car.update(elapsedTime);
        }

        if(this.player.isMovedUp()){
            player.move(0,-(player.getSpeedOfPlayer()));
            countAnimation++;
        }
        if(this.player.isMovedDown()){
            player.move(0,player.getSpeedOfPlayer());
            countAnimation++;
        }
        if(this.player.isMovedLeft()){
            player.move(-(player.getSpeedOfPlayer()),0);
            countAnimation++;
        }
        if(this.player.isMovedRight()){
            player.move(player.getSpeedOfPlayer(),0);
            countAnimation++;
        }
        // ...

        for (Car car : cars) {
            // falls car und player kollidieren
            // dann player zurück zum start (200, 500)

            int dx = Math.abs(player.getX() - car.getX());
            int dy = Math.abs(player.getY() - car.getY());
            //
            int w = player.getW() / 2 + car.getW() / 2;
            int h = player.getH() / 2 + car.getH() / 2;

            if (dx <= w && dy <= h) {
                // kollision!
                player.moveTo(153, 570);
                player.setNumberOfParcels(0); // Setzt bei Kollision zusätzlich diese Werte wieder auf Standard
                player.setSpeedOfPlayer(10);
                this.gameOver = true; // @Mareike Game Over Bild soll mit angezeigt werden.
            }
        }
    }

    /** Eine Methode um Musik abzuspielen.
     *
     * @param title nimmt einen URL als String an.
     */
    public void playMusic(String title) {
        String musicFile = title;
        URL fileURL = getClass().getResource(musicFile);

        Media media = new Media(fileURL.toString());
        GraphicsRightPanel.mediaPlayer = new MediaPlayer(media);
        GraphicsRightPanel.mediaPlayer.play();
        GraphicsRightPanel.mediaPlayer.setCycleCount(GraphicsRightPanel.mediaPlayer.INDEFINITE);
    }

    //Methode musste zusätzlich erstellt werden, da sich nur aufgerufen werden soll, wenn Key "E" gedrückt wird.
    //Im InputHandler kann gezielt diese Methode aufgerufen werden, wenn "E" gedrückt wird.

    public void handle() {
        //Die neue Abfrage prüft, ob sich der Player auf dem vorgesehen Platz befindet. Nur dann kann eine Aktion erfolgen.
        // Der Spieler muss komplett in der Zone sein und sich "mittig" am Objekt befinden.
        if(     getPlayer().getX() > 810 &&
                getPlayer().getX() < paketshop.getXPaketShop() &&
                getPlayer().getY()-37 + getPlayer().getH() < paketshop.getYPaketShop()+paketshop.getHPaketShop()) {
            player.takeAParcel();
        }
        if(     getPlayer().getX() > retourKutsche.getXRetourKutsche() &&
                getPlayer().getY()+35 - getPlayer().getH()> retourKutsche.getYRetourKutsche()) {
            player.deliverAParcel();
        }
    }

    //    Alter Code - Soll bleiben
//        //Pruefung, ob sich der Paketshop und der Player auf der gleichen Hoehe sind
//        int dx = Math.abs(player.getX() - paketshop.getXPaketShop());
//        int dy = Math.abs(player.getY() - paketshop.getYPaketShop());
//        //
//        int w = player.getW() / 2 + paketshop.getWPaketShop()/ 2;
//        int h = player.getH() / 2 + paketshop.getHPaketShop() / 2;
//
//        if (dx <= w  && dy <= h) {
//            // Pruefung, ob Paketshop und Player sich korrelieren
//            player.takeAParcel();
//        }
//
//        // Pruefung, ob die Retourkutsche & der Player auf der gleichen Hoehe sind
//        dx = Math.abs(player.getX() - retourKutsche.getXRetourKutsche());
//        dy = Math.abs(player.getY() - retourKutsche.getYRetourKutsche());
//        //
//        w = player.getW() / 2 + retourKutsche.getWRetourKutsche() / 2;
//        h = player.getH() / 2 + retourKutsche.getHRetourKutsche() / 2;
//
//        if (dx <= w  && dy <= h) {
//            // Pruefung, ob Paketshop und Player sich korrelieren
//            player.deliverAParcel();
//        }



    public int getCountAnimation() {
        return countAnimation;
    }

    public void setCountAnimation(int countAnimation) {
        this.countAnimation = countAnimation;
    }

    public boolean isGameOver() {
        return gameOver;
    }
}
