package de.awacademy.java.kollisionen.model;

import de.awacademy.java.kollisionen.CountdownTimer;
import de.awacademy.java.kollisionen.Highscore;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;

public class GraphicsRightPanel {

    private HBox root ;
    private Label anzahlBoxen;
    private Label headlineAnzahlBoxen;
    private Model model;
    private ImageView imageView;
    private CountdownTimer timerCD;
    private Label countdown;
    private Label highscore;
    private Label parcelCount;
    public static MediaPlayer mediaPlayer;
    private Label highscoreLabel;


    public GraphicsRightPanel(HBox root, Model model) {
        this.root = root;
        this.model = model;
        this.anzahlBoxen = new Label();
        this.highscoreLabel = new Label("HIGHSCORE: ");
        highscoreLabel.setFont(new Font("Cambria", 20));
        this.headlineAnzahlBoxen = new Label("Anzahl Boxen: ");
        headlineAnzahlBoxen.setFont(new Font("Cambria", 15));
        this.imageView = new ImageView();
        this.timerCD = new CountdownTimer();
        this.countdown = new Label("Verbleibende Zeit:");
        this.parcelCount = new Label();
        this.highscore = new Label(String.valueOf(Highscore.getInstance().getHighscore()));

    }

    /** Zeichnet den Rechten Panel.
     *
     * @return rechter Panel
     */
    public  VBox drawFirst() {
        VBox rechteLeiste = new VBox(20);
        HBox highscoreBox = new HBox(20);
        root.getChildren().add(rechteLeiste);
        highscoreBox.setAlignment(Pos.CENTER);
        rechteLeiste.getChildren().add(highscoreBox);
        highscoreBox.getChildren().add(highscoreLabel);
        highscoreBox.getChildren().add(highscore);
        rechteLeiste.getChildren().add(headlineAnzahlBoxen);
        rechteLeiste.getChildren().add(parcelCount);
        rechteLeiste.getChildren().add(countdown);
        rechteLeiste.getChildren().add(timerCD);

        rechteLeiste.getChildren().add(imageView);
        rechteLeiste.setPrefWidth(200);
        rechteLeiste.setAlignment(Pos.CENTER);
        return rechteLeiste;
    }

    /** Stellt das Bild von Carl auf der rechten Seite dar, je nach dem wie viele Boxen er trägt.
     *
     */
    public Image getCarlImage() {
        switch(model.getPlayer().getNumberOfParcels()) {
            case 0:
            return new Image(getClass().getResourceAsStream("/images/CarlRunningResized.png"));
            case 1:
            return new Image(getClass().getResourceAsStream("/images/Carl1KisteResized.png"));
            case 2:
            return new Image(getClass().getResourceAsStream("/images/Carl2KistenResized.png"));
            case 3:
            return new Image(getClass().getResourceAsStream("/images/Carl3KistenResized.png"));
            default:
                return null;
        }
    }

    /** Ändert den Inhalt der Felder im Rechten Panel
     *
     * @param rechteLeiste
     */
    public void drawRightPanel(VBox rechteLeiste) {
        parcelCount.setText(String.valueOf(Model.parcelCount));

        imageView.setImage(getCarlImage());
        imageView.setSmooth(true);
        imageView.setCache(true);

        rechteLeiste.setPadding(new Insets(10, 10, 10, 10));

        rechteLeiste.setStyle("-fx-background-color: #808080;");
    }


}
