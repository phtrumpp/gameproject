package de.awacademy.java.kollisionen.model;

public class AndereMauer {
    // Hier wird eine Mauer erstellt, welches als eingefärbtes Rechteck auf dem Canvas unseres Spiels zu sehen ist.
    // Damit wird gewährleistet, dass man nicht cheaten kann und durchgehend auf die Autos aufpassen muss
    private final int X = 180;
    private final int Y = 525;

    public int getXAndereMauer() { return X; }

    public int getYAndereMauer() { return Y; }

    public int getHAndereMauer() {
        return 70;
    }

    public int getWAndereMauer() {
        return 815;
    }
}
