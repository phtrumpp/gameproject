package de.awacademy.java.kollisionen.model;

public class PaketShop {

    // Wir bauen einen PaketShop, der als eingefärbtes Rechteck auf dem Canvas unseres Spiels zu sehen ist.
    private final int X = 845;
    private final int Y = 5;

    public int getXPaketShop() {
        return X;
    }

    public int getYPaketShop() {
        return Y;
    }

    public int getHPaketShop() {
        return 90;
    }

    public int getWPaketShop() {
        return 150;
    }
}
