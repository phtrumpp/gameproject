package de.awacademy.java.kollisionen.model;

public class BerlinerMauer {
    // Hier wird eine Mauer erstellt, welches als eingefärbtes Rechteck auf dem Canvas unseres Spiels zu sehen ist.
    // Damit wird gewährleistet, dass man nicht cheaten kann und durchgehend auf die Autos aufpassen muss
    private final int X = 5;
    private final int Y = 5;

    public int getXBerlinerMauer() { return X; }

    public int getYBerlinerMauer() { return Y; }

    public int getHBerlinerMauer() {
        return 90;
    }

    public int getWBerlinerMauer() {
        return 770;
    }
}
