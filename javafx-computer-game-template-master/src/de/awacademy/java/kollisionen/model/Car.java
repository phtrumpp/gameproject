package de.awacademy.java.kollisionen.model;

public class Car {
    private int x;
    private int y;
    private float speedX;

    public Car(int x, int y, float speedX) {
        this.x = x;
        this.y = y;
        this.speedX = speedX;
    }

    // Die Methode um Bedingungen ergänzt, so dass die Autos wieder am anderen Ende des Canvas ins Bild fahren.
    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedX);
        if (this.x > 1070) {
            this.x = 1;
        }
        if (this.x < -70) {
            this.x = 1000;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 70;
    }

    public int getW() {
        return 180;
    }

}
