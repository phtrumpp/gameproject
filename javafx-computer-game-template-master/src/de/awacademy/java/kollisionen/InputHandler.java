package de.awacademy.java.kollisionen;

import de.awacademy.java.kollisionen.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        //Das "Taste wird nur verarbeitet, wenn keine anderer Bewegung vorhanden ist.
        if (
                key == KeyCode.UP &&
                !model.getPlayer().isMovedRight() &&
                !model.getPlayer().isMovedLeft() &&
                !model.getPlayer().isMovedDown())
        {
                model.getPlayer().setMovedUp(true);
        } else if (
                key == KeyCode.DOWN &&
                !model.getPlayer().isMovedUp() &&
                !model.getPlayer().isMovedRight() &&
                !model.getPlayer().isMovedLeft())
        {
                model.getPlayer().setMovedDown(true);
        } else if (
                key == KeyCode.LEFT &&
                !model.getPlayer().isMovedUp() &&
                !model.getPlayer().isMovedRight() &&
                !model.getPlayer().isMovedDown())
        {
                model.getPlayer().setMovedLeft(true);
        } else if (
                key == KeyCode.RIGHT &&
                !model.getPlayer().isMovedLeft() &&
                !model.getPlayer().isMovedDown() &&
                !model.getPlayer().isMovedUp())
        {
            model.getPlayer().setMovedRight(true);
        } else if (key == KeyCode.E) {
            model.handle();
        }
    }

    public void onKeyReleased(KeyCode key) {
        if (key == KeyCode.UP) {
            this.model.getPlayer().setMovedUp(false);
            this.model.setCountAnimation(0);
        } else if (key == KeyCode.DOWN) {
            this.model.getPlayer().setMovedDown(false);
            this.model.setCountAnimation(0);
        } else if (key == KeyCode.LEFT) {
            this.model.getPlayer().setMovedLeft(false);
            this.model.setCountAnimation(0);
        } else if (key == KeyCode.RIGHT) {
            this.model.getPlayer().setMovedRight(false);
            this.model.setCountAnimation(0);
        }
    }
}
