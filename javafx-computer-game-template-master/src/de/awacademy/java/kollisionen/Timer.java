package de.awacademy.java.kollisionen;

import de.awacademy.java.kollisionen.model.GraphicsRightPanel;
import de.awacademy.java.kollisionen.model.Model;
import javafx.animation.AnimationTimer;
import javafx.scene.layout.VBox;

public class Timer extends AnimationTimer {
    private Model model;
    private Graphics graphics;
    private GraphicsRightPanel graphicsRightPanel;
    private VBox rechteLeiste;


    private long previousTime = -1;

    public Timer(Model model, Graphics graphics, GraphicsRightPanel graphicsRightPanel) {
        this.model = model;
        this.graphics = graphics;
        this.graphicsRightPanel = graphicsRightPanel;
        this.rechteLeiste = graphicsRightPanel.drawFirst();
    }

    @Override
    public void handle(long nowNano) {
        long nowMillis = nowNano / 1000000;

        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;

        model.update(elapsedTime);

        //Zeichnet den Canvas
        graphics.draw();

        //Zeichnet den rechten Panel
        graphicsRightPanel.drawRightPanel(rechteLeiste);

        if (CountdownTimer.getTmp() == 0 || model.isGameOver()) { // Abbruch des Draws über statische Getter von CountdownTimer
            this.stop();
            CountdownTimer.setTmp(0);
            graphics.drawGameOver(); //Öffnet neue Stage + Scene
        }
    }
}
