package de.awacademy.java.kollisionen;

import de.awacademy.java.kollisionen.model.*;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Graphics {
    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // BackgroundImage
        Image image = new Image(getClass().getResourceAsStream("/Images/Background.png"));
        gc.drawImage(image, 0, 0);

        //Einzelne Autos mit den jeweiligen Images werden erstellt
        Car car1 = this.model.getCars().get(0);
        gc.drawImage(
                new Image(getClass().getResourceAsStream("/images/Car_LightBlue.png")),
                car1.getX() - car1.getW() / 2,
                car1.getY() - car1.getH() / 2,
                car1.getW(),
                car1.getH());

        Car car2 = this.model.getCars().get(1);
        gc.drawImage(
                new Image(getClass().getResourceAsStream("/images/Car_red.png")),
                car2.getX() - car2.getW() / 2,
                car2.getY() - car2.getH() / 2,
                car2.getW(),
                car2.getH());

        Car car3 = this.model.getCars().get(2);
        gc.drawImage(
                new Image(getClass().getResourceAsStream("/images/Car_Blau.png")),
                car3.getX() - car3.getW() / 2,
                car3.getY() - car3.getH() / 2,
                car3.getW(),
                car3.getH());

        Car car4 = this.model.getCars().get(3);
        gc.drawImage(new Image(getClass().getResourceAsStream("/images/Car_Dark.png")),
                car4.getX() - car4.getW() / 2,
                car4.getY() - car4.getH() / 2,
                car4.getW(),
                car4.getH());

        // Animation "nachmachen"
        //Wenn der Player nach oben geht!
        if (model.getPlayer().isMovedUp()) {
            animationDesPlayers("/images/CarlMove01.png","/images/CarlMove02.png","/images/CarlMove03.png");
            //Wenn der Player nach rechts geht
        } else if (model.getPlayer().isMovedRight()) {
            animationDesPlayers("/images/CarlMove01Right.png","/images/CarlMove02Right.png","/images/CarlMove03Right.png");
            //Wenn der Player nach links geht
        } else if (model.getPlayer().isMovedLeft()) {
            animationDesPlayers("/images/CarlMove01Left.png","/images/CarlMove02Left.png","/images/CarlMove03Left.png");
            //Wenn der nach unten geht
        } else if (model.getPlayer().isMovedDown()) {
            animationDesPlayers("/images/CarlMove01Down.png","/images/CarlMove02Down.png","/images/CarlMove03Down.png");
            //Wenn der Player sich nicht bewegt.
        } else if (model.getCountAnimation() == 0) {
            bildDesPlayers("/images/CarlMove04.png");
        }

        //Erste Lösung der Animation. Spieler bewegt sich immer.
//        if (count <= 4) {
//            Player p = model.getPlayer();
//            gc.drawImage(new Image(getClass().getResourceAsStream("/images/CarlMove01.png")),p.getX() - p.getW()/2,p.getY() - p.getH()/2,p.getW(), p.getH());
//            count++;
//        } else if (count <= 9) {
//            Player p = model.getPlayer();
//            gc.drawImage(new Image(getClass().getResourceAsStream("/images/CarlMove02.png")),p.getX() - p.getW()/2,p.getY() - p.getH()/2,p.getW(), p.getH());
//            count++;
//        } else if (count <= 14) {
//            Player p = model.getPlayer();
//            gc.drawImage(new Image(getClass().getResourceAsStream("/images/CarlMove03.png")),p.getX() - p.getW()/2,p.getY() - p.getH()/2,p.getW(), p.getH());
//            count++;
//        } else if (count <= 18) {
//            Player p = model.getPlayer();
//            gc.drawImage(new Image(getClass().getResourceAsStream("/images/CarlMove04.png")),p.getX() - p.getW()/2,p.getY() - p.getH()/2,p.getW(), p.getH());
//            count++;
//        } else if (count == 19) {
//            Player p = model.getPlayer();
//            gc.drawImage(new Image(getClass().getResourceAsStream("/images/CarlMove04.png")),p.getX() - p.getW()/2,p.getY() - p.getH()/2,p.getW(), p.getH());
//            count=0;
//        }


//        // Wir fügen die Graphics unseres PaketShops hinzu
//        gc.setFill(Color.CADETBLUE);
//        gc.fillRect(
//                paketShop.getXPaketShop(),
//                paketShop.getYPaketShop(),
//                paketShop.getWPaketShop(),
//                paketShop.getHPaketShop()
//        );
//
//        // Wir fügen die Graphics unseres Paket-Autos RetourKutsche hinzu
//        gc.setFill(Color.YELLOWGREEN);
//        gc.fillRect(
//                retourKutsche.getXRetourKutsche(),
//                retourKutsche.getYRetourKutsche(),
//                retourKutsche.getWRetourKutsche(),
//                retourKutsche.getHRetourKutsche()
//        );
//        // Wir fügen die Graphics unserer "AndererMauer" hinzu
//        gc.setFill(Color.BLUEVIOLET);
//        gc.fillRect(
//                andereMauer.getXAndereMauer(),
//                andereMauer.getYAndereMauer(),
//                andereMauer.getWAndereMauer(),
//                andereMauer.getHAndereMauer()
//        );
//
//        // Wir fügen die Graphics unserer "BerlinerMauer" hinzu
//        gc.setFill(Color.BLACK);
//        gc.fillRect(
//                berlinerMauer.getXBerlinerMauer(),
//                berlinerMauer.getYBerlinerMauer(),
//                berlinerMauer.getWBerlinerMauer(),
//                berlinerMauer.getHBerlinerMauer()
//        );
    }

    /**
     *  Die Methode, welche den GameOver Screen erstellt
     */
    public void drawGameOver() {
        Stage stage = new Stage();
        VBox root = new VBox(20);
        root.setPadding(new Insets(50,10,10,10)); // Padding Abstand
        Scene scene = new Scene(root, 400, 400);
        String css = this.getClass().getResource("/css/stylesheet.css").toExternalForm();
        scene.getStylesheets().add(css);

        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);

        Label highscoreLabel = new Label();
        Label deinScore = new Label();
        Label gameOverLabel = new Label();

        deinScore.setText("Dein Highscore: ");
        gameOverLabel.setText("GAME OVER");

        String highscore = Integer.toString(Model.parcelCount); // Score als String übergeben
        highscoreLabel.setText(highscore);

        highscoreLabel.setAlignment(Pos.CENTER);
        gameOverLabel.setAlignment(Pos.CENTER);

        root.getChildren().add(gameOverLabel);
        root.getChildren().add(deinScore);
        root.getChildren().add(highscoreLabel);

        /*

        Button spielNeustarten = new Button("Versuchs nochmal!");

        spielNeustarten.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                // ==== Spiel neustarten? ===== //
                // ==== Gleicher Launch Prozess funktioniert nicht =====//



            }
        });


         */
    }

    /** Zeigt das Bild des Players
     *
     * @param pfadVomImage Pfad Bild
     */
    private void bildDesPlayers(String pfadVomImage) {
        Player p = model.getPlayer();
        gc.drawImage(new Image(getClass().getResourceAsStream(pfadVomImage)), p.getX() - p.getW() / 2, p.getY() - p.getH() / 2, p.getW(), p.getH());
    }

    /**
     * Spielt 3 Bilder des Spielers hintereinander ab wenn er sich bewegt
     * @param pfadVomImage1 Pfad Bild 1
     * @param pfadVomImage2 Pfad Bild 2
     * @param pfadVomImage3 Pfad Bild 3
     */
    private void animationDesPlayers(String pfadVomImage1, String pfadVomImage2, String pfadVomImage3) {
            if (model.getCountAnimation() <= 3) {
                bildDesPlayers(pfadVomImage1);
                model.setCountAnimation(model.getCountAnimation() + 1);
            } else if (model.getCountAnimation() <= 7) {
                bildDesPlayers(pfadVomImage2);
                model.setCountAnimation(model.getCountAnimation() + 1);
            } else if (model.getCountAnimation() <= 10) {
                bildDesPlayers(pfadVomImage3);
                model.setCountAnimation(model.getCountAnimation() + 1);
            } else if (model.getCountAnimation() == 11) {
                bildDesPlayers(pfadVomImage3);
                model.setCountAnimation(0);
            }
    }
}