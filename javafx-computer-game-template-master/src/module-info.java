module javafx.computer.game.template.master {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.media;

    opens de.awacademy.java.kollisionen;
}