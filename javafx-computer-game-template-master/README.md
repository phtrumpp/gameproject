# JavaFX Computer Game Pattern
## Get Started
- Ensure JavaFX libraries are available. Newer JDK version might exclude javafx.
- Run Main class
- You'll see a computer game
- Use arrow keys (up, down, left, right) to move the square.

This is a TEMPLATE to be used (not a fully functioning FX application yet).



